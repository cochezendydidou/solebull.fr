---
title: Fortnite
playlist: PLk_fUHaG1xocKOYJl_t1hHT3r3M2YkAZn
layout: /game.*
menu: games
---

## Fortnite Battle Royale ##

Fortnite est un jeu en ligne développé et édité en 2017 par Epic Games.
Nous ne jouons qu'au mode *Battle Royale*. Son principal intérêt est de pouvoir
jouer avec les joueurs d'autre plateformes (*cross-play*).

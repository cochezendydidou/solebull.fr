---
title: "Minetest-pvp | Solebull et SniperGames"
menu: minetest-pvp
---

Nous développons un jeu, un ensemble de *mods* pour minetest en PVP
(joueur contre joueur). Veuillez utiliser la 
[version 5.0.1](https://www.minetest.net/downloads/) du client pour
vous connecter.

Merci a Exaltion pour l'hebergement du serveur.

# Comment jouer
 
 * Telechargez la version 5.0 de minetest : [minetest.net/](https://www.minetest.net)
 * Connectez vous au serveur
   * IP : 185.41.154.95 
   * Port : 3000
 * Pour la partie vocale nous utilisons [mumble](https://wiki.mumble.info/wiki/Main_Page) : 
   * Adresse : mumble13.omgserv.com 
   * Port 19739

# Développement

Le projet de développement des mods se trouve chez *gitlab* : 
[gitlab.com/solebull/minetest-pvp](https://gitlab.com/solebull/minetest-pvp). Vous pouvez nous aider en soumettant des bugs 
[ici](https://gitlab.com/solebull/minetest-pvp/issues).

# Mapper

Le *mapper* est un programme C++ qui génère la carte globale du serveur. Il
doit être modifié poutr afficher les factions actuelles. Son développement
se déroule a l'addresse 
[gitlab.com/solebull/minetestmapper-factions](https://gitlab.com/solebull/minetestmapper-factions).

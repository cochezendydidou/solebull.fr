---

title: "Planning | Solebull et SniperGames"
layout: /planning.*
menu: planning

games:
- [29 Juillet 2019, "Osu!", "twitch"]
- [30 Juillet 2019, "Rocket League", "yt"]
- [31 Juillet 2019, "Fortnite", "yt"]
- [ 1 Août 2019, "", "rest"]
- [ 2 Août 2019, "Lemmings", "yt"]
- [ 3 Août 2019, "Minecraft", "yt"]
---

Voici le planning de la semaine. Les lives se déroulent generalement de 18h à 
22h, heures de Paris. Ne nous demandez pas de changer de jeu.

Il m'arrive quelque fois de changer en cours de soirée pour faire
du retro-gaming ou vous présenter des jeux libres.

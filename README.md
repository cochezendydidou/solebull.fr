# solebull.fr

Un (très) simple site web de présentation pour noter activité de *streamers*.

## Installation

Copier le contenu du répertoire **www/** dans la racine de votre hébergement
web.

## Nanoc version

If your system doesn't provide ruby 2.4 (for example, Debian Stretch), you
must install ruby 2.4 before installing dependencies :

	sudo apt-get install curl 
	curl -sSL https://rvm.io/mpapis.asc | sudo gpg --import -
	curl -sSL https://get.rvm.io | sudo bash -s stable
	su 
	#source /etc/profile.d/rvm.sh
	#rvm requirements
	#rvm list known
	#rvm install 2.4
	#sudo chmod a+w -R /usr/local/rvm/gems/ruby-2.4.4/wrappers
	exit
	rvm use 2.4.4 --default

To install needed gems, especially *nanoc* one, you must to run :

   	sudo apt install bundler
	bundle

Then build and test your website :

	cd src
	nanoc
	nanoc view

# Configuration

The *minetest-pvp* page can be hiddeen in `src/nanoc.yaml`, with the 
**minetest_show** value.
